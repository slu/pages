#!/usr/bin/env bash

set -euxo pipefail

tempdir=$(mktemp -d); trap 'rm -rf -- "$tempdir"' EXIT

git worktree add "$tempdir" pages

hugo --destination "$tempdir"

(
    cd "$tempdir" || exit
    git add -A && git commit -m "Build at $(date -Is)"
)

git push origin pages
