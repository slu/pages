[![Build Status][ci-badge]][ci-project] [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]


# SLU's Mountain of Code

This repository holds the content used to serve my Codeberg-centric
website at https://slu.codeberg.page/


## License

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License][cc-by-sa].


[ci-project]: https://ci.codeberg.org/repos/13654
[ci-badge]: https://ci.codeberg.org/api/badges/13654/status.svg

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
