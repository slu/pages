---
title: "About Me"
date: 2024-08-14T10:29:41+02:00
url: about
_build:
  list: never
---

Allow me to introduce myself. My name is Søren Lund, and I also go by
the handle `slu`. I live in Denmark and have been dabbling with
computers and programming for many years, both at home for fun, at
university, and professionally.


I am a versatile IT consultant with over twenty-five years of
professional experience. I have a wide range of competencies and like
new challenges. I am, in somewhat prioritized order

* an AWS Solution Architect
* a DevOps Engineer
* a Full Stack Developer
* a Computer generalist
* a Linux User
* a Perl Monger
* a Pythonista
* an Emacs Evangelist, and
* a Java Developer
