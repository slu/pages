---
title: "My Projects"
tags: [projects, repositories, personal]
date: 2024-08-15T10:46:23+02:00
weight: 1
---

You've stumbled upon the entrance to the projects I have on
[codeberg.org]. All projects are [open-source software] and most are
released under a [GPL3 or later license].


## Repositories

Below is a list of some repositories I have on [codeberg.org].

- [aws-utils-for-bash] — A small collection of Bash scripts,
  functions, and aliases complimenting the AWS CLI for an enhanced
  experience.
- [dot-tmux-conf] — My configuration file for tmux.
- [hekla] — Little experiment creating bar graphs from an algorithm
  that converts an n-fold Cartesian product into a list of numbers.
- [home-repair] — Script to check your $HOME directory.
- [install-check] — Script that can check versions of installed
  software.
- [mandelbrot] — Mandelbrot set-generating programs implemented in
  different programming languages.
- [minime] — Scripts and notes for my framebuffer-only Linux laptop
  computer.
- [pages] — The source of this website.
- [retro-games] — My recreations of old games for the terminal.


<!-- References / Links -->

[codeberg.org]: https://codeberg.org/slu
[open-source software]: https://en.wikipedia.org/wiki/Open-source_software
[GPL3 or later license]: https://www.gnu.org/licenses/gpl-3.0.en.html

[aws-utils-for-bash]: https://codeberg.org/slu/aws-utils-for-bash
[dot-tmux-conf]: https://codeberg.org/slu/dot-tmux-conf
[hekla]: https://codeberg.org/slu/hekla
[home-repair]: https://codeberg.org/slu/home-repair
[install-check]: https://codeberg.org/slu/install-check
[mandelbrot]: https://codeberg.org/slu/mandelbrot
[minime]: https://codeberg.org/slu/minime
[pages]: https://codeberg.org/slu/pages
[retro-games]: https://codeberg.org/slu/retro-games
